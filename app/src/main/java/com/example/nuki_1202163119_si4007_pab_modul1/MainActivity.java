package com.example.nuki_1202163119_si4007_pab_modul1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private TextView panjang ,lebar,hasil;
    private Button tombol;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        panjang =   findViewById(R.id.panjang);
        lebar   =   findViewById(R.id.lebar);
        hasil   =   findViewById(R.id.hasil);
        hasil   =   findViewById(R.id.hasil);
        tombol  =   findViewById(R.id.button);

        hasil.setText("");
        tombol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int getPanjang = Integer.parseInt(panjang.getText().toString());
                int getLebar = Integer.parseInt(lebar.getText().toString());
                int rumus = getPanjang*getLebar;
                hasil.setText(String.valueOf(rumus));
            }
        });
    }

}
